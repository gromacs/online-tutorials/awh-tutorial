{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Execute this first \n",
    "#\n",
    "#  * trigger notebook styling\n",
    "#  * check if notebook had been modified since its distribution\n",
    "# \n",
    "# Note: executing any cells before this modifies the notebook.\n",
    "# \n",
    "%run src/init_notebooks.py\n",
    "#hide_toggle()\n",
    "#check_notebook()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Calculating PMFs with AWH in GROMACS\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "    Author:   Viveca Lindahl (reviewed/updated by Alessandra Villa and Christian Blau)\n",
    "    Goal:     Learn how to calculate potential of mean forces with accelerated weight histogram (AWH) method in GROMACS\n",
    "    Prerequisites: Know how to run an MD simulation, basis knowledge of using a Linux shell, basis knowledge of thermodynamics and awareness of accelerated weight histogram method (see reference below).\n",
    "    Time:      80 minutes\n",
    "    Software:  GROMACS 2023 (2022, 2021, 2020, 2019, 2018), python modules: numpy, matplotlib, re, nglviewer (https://nglviewer.org/) \n",
    "    Optional Software: visualization software [VMD](https://www.ks.uiuc.edu/Research/vmd), Xmgrace plotting tool\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here we learn how to calculate the potential of mean force\n",
    "(PMF) along a reaction coordinate (RC) using the accelerated weight\n",
    "histogram method (AWH) in GROMACS.  We will go through both how to set\n",
    "up the input files needed, as well as how to extract and analyze the\n",
    "output after having run the simulation. \n",
    "\n",
    "AWH applies a time-dependent bias potential along the chosen RC, which is tuned during the simulation\n",
    "such that it \"flattens\" the barriers of the PMF to improve sampling along the RC.\n",
    "With better sampling, the PMF can be calculated more accurately than using unbiased MD.\n",
    "\n",
    "## References\n",
    "For more details about the AWH method and how it can be applied we refer to\n",
    "* The GROMACS reference manual section about AWH, available on [the GROMACS documentation website](http://manual.gromacs.org/current/reference-manual/special/awh.html)\n",
    "* BioExcel Webinar: [Accelerating sampling in GROMACS with the AWH method](https://bioexcel.eu/webinar-accelerating-sampling-in-gromacs-with-the-awh-method-2019-09-24/)\n",
    "* Lindahl, V., Lidmar, J. and Hess, B., 2014. Accelerated weight histogram method for exploring free energy landscapes. *The Journal of chemical physics*, 141(4), p.044110. [doi:10.1063/1.4890371](https://doi.org/10.1063/1.4890371)\n",
    "* Lindahl, V., Gourdon, P., Andersson, M. and Hess, B., 2018. Permeability and ammonia selectivity in aquaporin TIP2;1: linking structure to function. *Scientific reports*, 8(1), p.2995. [doi:10.1038/s41598-018-21357-2](https://doi.org/10.1038/s41598-018-21357-2)\n",
    "* Lindahl, V., Villa, A. and Hess, B., 2017. Sequence dependency of canonical base pair opening in the DNA double helix. *PLoS computational biology*, 13(4), p.e1005463. [doi:10.1371/journal.pcbi.1005463](https://doi.org/10.1371/journal.pcbi.1005463)\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The case study: DNA base pair opening\n",
    "We will calculate a PMF for opening a DNA base\n",
    "pair. The DNA double helix is a very stable structure. Opening a base pair\n",
    "requires breaking hydrogen bonds between the bases and crossing a high free energy\n",
    "barrier.  That's why we need to enhance the sampling by applying a bias!\n",
    "<img src=\"figs/dna-helix.png\" alt=\"dna\" style=\"height: 300px;\"/>\n",
    "As our reaction coordinate we use the distance between the two atoms forming the central hydrogen-bond the two bases in a pair. \n",
    "\n",
    "Let's have a look at the system and the reaction coordinate. We have hidden all the water to better see the DNA itself, and we have put punchy colors on the atoms defining the RC of our target base pair. Now run this and the DNA should pop up:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import nglview as ng\n",
    "view = ng.show_file(\"visualization/dna-centered.pdb\", default=False)\n",
    "view.center(selection=\"DA or DT\")\n",
    "view.add_representation(\"cartoon\")\n",
    "view.add_representation(\"base\", selection=\"not DA and not DT\")\n",
    "view.add_representation(\"ball+stick\",selection=\"DA\")\n",
    "view.add_representation(\"ball+stick\",selection=\"DT\")\n",
    "view.add_representation(\"ball+stick\",selection=\"DA and .N1\", color=\"green\")\n",
    "view.add_representation(\"ball+stick\",selection=\"DT and .N3\", color=\"green\")\n",
    "view\n",
    "# click and drag to rotate, zoom with your mouseweel \n",
    "# for more infor on this viewer have a look at https://github.com/nglviewer/nglview"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Rotate the structure and look for the two (nitrogen) atoms in green. The distance between these will serve as our reaction coordinate for that base pair."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In alternative, you can use VMD  to visualize the reaction coordinate on your local machine. The `-e` flag below tells VMD to excute the commands that are in the following .tcl script. These commands change how the system is visually represented. For instance, we have hidden all the waters to better see the DNA itself, and we have put punchy colors on the atoms   defining the RC of our target base pair. To run VMD from within this notebook, remove the comment character (#) in the following cell and VMD should pop up:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#!vmd visualization/dna-centered.gro -e visualization/representation.tcl"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Rotate the structure and look for the two (nitrogen) atoms in green. The distance between these will serve as our RC for that base pair.\n",
    "Quit VMD to continue."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The MD parameter file \n",
    "We'll now assume we have already built and equilibrated the system, so we are almost ready to go. To use AWH we basically just need to add some extra parameters in the .mdp file. Go to and check out the directory that has all the run files of our first AWH example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%cd awh-simple\n",
    "!ls -l"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Find the differences between the mdp file using AWH and an mdp file for a vanilla MD simulation:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!diff no-awh.mdp awh.mdp"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here '<' refers to the content of the first argument to `diff` (the non-AWH .mdp file) and '>' to the second (the AWH .mdp file). E.g., we increased the number of steps (`nsteps`) for AWH. The more relevant parameters are the ones prefixed `pull` and `awh`. The awh-`.mdp` options are documented at [https://manual.gromacs.org/current/user-guide/mdp-options.html#mdp-awh](https://manual.gromacs.org/current/user-guide/mdp-options.html#mdp-awh)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The index file\n",
    "The .mdp file now depends on some definitions of atom groups, `base_N1orN3` and `partner_N1orN3`. We need to make an index (.ndx) file defining these and provide as input when generating a .tpr file with `gmx grompp`. Here our groups are as simple as they get: each group contains a single nitrogen atom (with names 'N1' or 'N3') . But don't get tempted to edit an index file manually! The traditional tool to use is `gmx make_ndx` and a more general and powerful tool is `gmx select`. We focus on AWH here and have provided the index file. Double-check that the pull groups (e.g. `pull-group1-name`) from the .mdp file are actually defined in the index file:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!grep -A 1  N1orN3  index.ndx  # '-A 1' to show also 1 line after the match "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "One atom per group looks right. In a real study, a better check could be to visualize the atom indices (e.g. with VMD)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Starting the simulation\n",
    "Here we won't run the actual simulation; a directory with data has already been provided. But for practice, we will generate a tpr, as usual with `grompp`. The flag `-quiet` tells `grompp` to be less verbose."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!gmx grompp -quiet -f awh.mdp -n index.ndx -p topol.top -o topol.tpr"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that the values of the pull coordinate values in the provided .gro file are printed by `grompp`. Does it look reasonable? "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Check the log file\n",
    "Now let's look at the data we've provided. Some information related to the AWH initial convergence can be found in the `mdrun` log file."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "!grep 'covering' data/md.log\n",
    "!grep 'out of the' data/md.log"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here, a \"covering\" refers to an end-to-end transition across the sampling interval [`awh1-dim1-start` , `awh1-dim1-end`]. Note that the first transitions are faster than the later ones. Why is that?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The AWH and initial stage and convergence\n",
    "The overall update size of the free energy estimate, here denoted by $|\\Delta F|$, is a free parameter in AWH. It is very important because converging the free energy relies on $|\\Delta F|\\to 0$ for large times $t$. Also, the time-dependent bias function is a direct function of the current free energy estimate. Thus, initially $|\\Delta F|$ has to be large enough that the bias fluctuations efficiently promote escaping free energy minima.\n",
    "\n",
    "In the AWH *initial stage*, $|\\Delta F|$, is kept constant *during* each transition across the sampling interval. After the interval has been covered, the update size is scaled down by a constant factor. This leads to a stepwise decrease of the update size in the initial stage, which approaches an exponential decay $|\\Delta F|\\sim 1/e^{\\alpha t}$. After exiting this stage, the update size instead decreases steadily with time, $|\\Delta F| \\sim 1/t$. The latter has proven to be the right long-term time evolution for the free energy to converge, but if applied from the start it generally decreases the update size too quickly. The exit time is determined dynamically by AWH.  The exit of the initial stage should occur before the initial exponential-like decay of $|\\Delta F|$ becomes more rapid than $\\sim1/t$.\n",
    "\n",
    "The initial stage is a type of \"burn-in\" procedure designed to improve the robustness of the method. The idea is to first get a quick and rough free energy estimate in the initial stage, followed by refinement in the final stage. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The reaction coordinate trajectory\n",
    "The trajectory of the pull coordinate is found in `pullx.xvg`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%less data/pullx.xvg"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For plotting and data analysis we provide a Python function to read the `.xvg` file format into this notebook."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "# Function that reads data from an .xvg file\n",
    "def read_xvg(fname):\n",
    "    data=[]\n",
    "    with open(fname) as f: \n",
    "        for line in f:\n",
    "            # Lines with metadata or comments start with #, @\n",
    "            if not line.startswith((\"@\",\"#\")):\n",
    "                data.append(np.array([float(s) for s in line.split()]))\n",
    "    data = np.vstack(data)\n",
    "    return data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can for instance plot the covering times."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Extract substrings using regular expressions\n",
    "import re\n",
    "\n",
    "# Read pullx.xvg file and plot a selected part of the trajectory\n",
    "rundir='data/'\n",
    "data = read_xvg(rundir + \"/pullx.xvg\")\n",
    "t, x = data[:,0], data[:,1]\n",
    "\n",
    "fig, ax=plt.subplots(figsize=(16,10))\n",
    "plt.plot(t, x, lw=1)\n",
    "\n",
    "# Set y-axis limits based on the chosen sampling interval \n",
    "awh_start, awh_end = 0.25, 0.65    \n",
    "plt.ylim([awh_start, awh_end])\n",
    "ymin, ymax = plt.gca().get_ylim()\n",
    "\n",
    "# Find covering times plot them\n",
    "covering_logs = !grep covering {rundir}/md.log\n",
    "covering_times = [float(re.findall('t = (.+?) ps', log)[0]) for  log in covering_logs]\n",
    "\n",
    "for tcover, log in zip(covering_times, covering_logs):\n",
    "    plt.plot([tcover]*2,[ymin,ymax], color='k', lw=2, ls='-')\n",
    "\n",
    "# Also mark the exit time if there was one\n",
    "exit_logs = !grep 'out of the' {rundir}/md.log\n",
    "if len(exit_logs) > 0:\n",
    "    texit = float(re.findall('t = (.+?)', exit_logs[0])[0])\n",
    "    plt.plot([texit],0.5*(ymin + ymax), color='k', marker='x', ms=10)\n",
    "\n",
    "# Tweak more\n",
    "tend=tcover*2\n",
    "plt.xlim([0,min(tend,max(t))])\n",
    "plt.xlabel(\"time (ps)\")\n",
    "plt.ylabel(\"distance (nm)\");\n",
    "plt.title('RC trajectory');"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here, the black vertical lines indicate covering times and the $\\times$ marks the exit time, extracted from `md.log`. Verify that these coincide roughly with the transition times seen in the trajectory. What we expect to see here is that the covering times are shorter initially, when the free energy estimate and bias fluctuates more, and then increase and reach a constant average value as the bias stops changing. You should always check this trajectory and make sure that there are at least a few transitions after the initial stage. If not, you need to simulate longer or you have a poorly chosen RC that is not guiding the transitions efficiently."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The `gmx awh` tool and the `awh.xvg` files"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Data directly related to AWH is extracted using `gmx awh`. The data is stored in the energy file, `ener.edr`. The default output file name is of the format `awh_t<time>.xvg`; there will be one file for each AWH output time, or less if you use the `-skip` flag. Let's dump the output into a separate directory."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!ls awh-data"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!mkdir -p awh-data\n",
    "!gmx awh -quiet -s topol.tpr -more -f data/ener.edr -kt -o awh-data/awh.xvg"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!ls awh-data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Above, the `-more` flag tells `gmx awh` to extract more than the PMF from the AWH files, e.g. histograms. The default `gmx` unit of energy is kJ/mol, but with the flag `-kT` we get units of kT."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's have a quick look at the contents of one of these files (from time 100 ns). You can use `xmgrace` on your local machine to plot the file, by removing the comment character (#) in the following cell to do that. That will provide a detailed legend that helps understanding what data is being plotted. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "t=100000 # ps\n",
    "# -nxy to plot multicolumn data\n",
    "awh_data = read_xvg(\"awh-data/awh_t{}.xvg\".format(t)).T\n",
    "\n",
    "fig,ax = plt.subplots()\n",
    "ax.plot(awh_data[0],awh_data[1])\n",
    "ax.set_xlabel(\"RC(nm)\")\n",
    "ax.set_ylabel(\"PMF(kT)\")\n",
    "\n",
    "# uncomment the line below if you have xmgrace installed\n",
    "# !xmgrace -free -nxy awh-data/awh_t{t}.xvg # wait for it... "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here you can have a look at the raw-data:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%less awh-data/awh_t100000.xvg"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Scrolling down to `@ slegend ...` gives you more details.\n",
    "We list the entries in these files in more detail here:\n",
    "\n",
    "|column|title|description\n",
    "|------|-----|-----------\n",
    "|1|PMF|the free energy estimate along the RC, $\\xi(x)$\n",
    "|2|Coord bias|the bias acting on  $\\xi$. Note: $\\xi$ is *attracted* to large values of this bias *function*. The bias *potential* has opposite sign\n",
    "|3|Coord distr| histogram of  $\\xi$\n",
    "|4|Ref value distr| probability weight histogram of the reference coordinate value, $\\lambda$\n",
    "|5|Target ref value distr| the target distribution for $\\lambda$, uniform by default (set by .mdp parameter `awh1-target`)\n",
    "|6|Friction metric| a metric for local friction, or 1/diffusion (here normalized and unitless). Can pinpoint hard-to-sample regions"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Inspecting the results in more detail"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig,ax = plt.subplots(3,2, figsize=(16,10), sharex=True)\n",
    "ax[0,0].plot(awh_data[0],awh_data[1])\n",
    "ax[0,0].set_xlabel(\"RC(nm)\")\n",
    "ax[0,0].set_ylabel(\"PMF(kT)\")\n",
    "\n",
    "ax[0,1].plot(awh_data[0],awh_data[2])\n",
    "ax[0,1].set_xlabel(\"RC(nm)\")\n",
    "ax[0,1].set_ylabel(\"Coord bias\")\n",
    "ax[1,0].plot(awh_data[0],awh_data[3])\n",
    "ax[1,0].set_xlabel(\"RC(nm)\")\n",
    "ax[1,0].set_ylabel(\"Coord distr\")\n",
    "ax[1,1].plot(awh_data[0],awh_data[4])\n",
    "ax[1,1].set_xlabel(\"RC(nm)\")\n",
    "ax[1,1].set_ylabel(\"Ref distr\")\n",
    "\n",
    "ax[2,0].plot(awh_data[0],awh_data[5])\n",
    "ax[2,0].set_xlabel(\"RC(nm)\")\n",
    "ax[2,0].set_ylabel(\"Target ref distr\")\n",
    "ax[2,1].plot(awh_data[0],awh_data[6])\n",
    "ax[2,1].set_xlabel(\"RC(nm)\")\n",
    "ax[2,1].set_ylabel(\"Friction\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Sanity checks for the distributions\n",
    "(zoom in to see them better)\n",
    "* *Coord distr* $\\approx$ *Ref value distr*, or the force constant (.mdp parameter `awh1-dim1-force-constant`), which couples $\\lambda$ to $\\xi$, may be too weak.\n",
    "\n",
    "Is this true everywhere in our case? E.g., for the smallest distances, where atoms are repelling each other and the PMF is very steep, it doesn't hold entirely! In general, such end effects can be expected and are not critical. Compare also the PMF and the bias function at the ends!   \n",
    "\n",
    "* *Ref value distr* $\\approx$ *Target ref value distr*, i.e. that what we sampled is close to the distribution that we targeted. If not true, we may have a poor bias estimate/need to sample longer.\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Evolution of the PMF estimate\n",
    "Now we look closer at the time-evolution of the PMF (you can do the same for either variable in the awh.xvg file)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Plot PMFs from AWH xvg files\n",
    "\n",
    "# A list of all AWH files\n",
    "fnames = !ls awh-data/awh_t*xvg\n",
    "\n",
    "# Extract time of each file\n",
    "times = [float(re.findall('awh_t(.+?).xvg', fname)[0]) for fname in fnames]\n",
    "\n",
    "# Sort the files chronologically\n",
    "fnames = [f for (t,f) in sorted(zip(times, fnames))]\n",
    "times.sort()\n",
    "print(\"Number of files:\", len(fnames))\n",
    "print(\"Time in between files \", times[1] - times[0], 'ps')\n",
    "print(\"Last time\", times[-1], 'ps')\n",
    "\n",
    "# Plot the PMF from first files/times\n",
    "labels=[]\n",
    "istart = 0 # Start plotting from this file index\n",
    "nplot = 10  # Number of files to plot\n",
    "for fname in fnames[istart:istart+nplot]:\n",
    "    data=read_xvg(fname)\n",
    "    labels.append(re.findall('awh_t(.+?).xvg', fname)[0] + ' ps') # use the time as label\n",
    "    plt.plot(data[:,0], data[:,1])\n",
    "plt.xlabel('distance (nm)')\n",
    "plt.ylabel('PMF (kT)')\n",
    "plt.xlim([0.25,0.60])\n",
    "plt.ylim([0,20])\n",
    "plt.legend(labels);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Try increasing the `istart` variable above to see how the PMF estimates are changing (less and less) over time. *Note*: this convergence does not easily translate into error estimates for the PMF. To get such error bars the simplest is to run multiple (independent) AWH simulations and calculate standard errors from that."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Congratulations! You are now ready to run an AWH simulation in GROMACS! Let's go back to the directory where we started and continue with a somewhat more advanced setup."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##  Bias sharing walkers\n",
    "A simple way of parallelizing sampling with AWH is to launch multiple simultaneous trajectories, so called \"walkers\", and have them share a common bias and free energy estimate. This can reduce the length of the initial stage significantly. Running `mdrun -multidir` with our previous .tpr files would launch multiple *independent* simulations. To make the AWH biases in the simulations aware of each other, and share information, we need to add a couple of things in the .mdp file."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%cd ..\n",
    "%cd awh-multi\n",
    "!ls -l"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!diff  ../awh-simple/awh.mdp awh-multi.mdp"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here, `awh-share-multisim` is just a switch to turn on the multisim sharing.\n",
    "What is `awh1-share-group`? Each bias that shares its information with another bias needs to belong to a so called `share-group`, indexed by $1, 2,\\ldots$. The reason is to remove the ambiguity arising when are multiple AWH biases present within one simulation (e.g. one bias per monomer of a multimeric protein). Since we have only one bias here, setting this parameter is trivial.\n",
    "Finally, we switched on `awh1-equilibrate-histgram`. This will add an \"equilibration\" stage before the initial stage making sure that the sampled distribution approximately equals the target distribution before attempting to decrease the update size. We'll see this in action soon.\n",
    "\n",
    "From a user-perspective, the main difference now is that the input and output has been multiplied by the number of participating walkers. This is true except for the main AWH output, which is shared!. Thus, you only need to run `gmx awh` on one of the `ener.edr` files, they all contain the same information. The friction metric is the exception here, which currently is not shared for implementation reasons.\n",
    "\n",
    "Let's looks closer at the data we have been given"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!ls -l data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "and the contents of these run directories "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!ls data/?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To figure out how this data was generated do"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!grep Command -A 1 data/0/md.log"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that for `-multidir` you need a GROMACS build supporting MPI communication, `gmx_mpi`. The option -v means verbose, avoid to use this option when you run on HPC cluster.  "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The initial stage for multiple walkers\n",
    "We can repeat more or less the same analysis as we did in the single-walker case"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#`-e` allows us to specify multiple search patterns. \n",
    "!grep -e 'equilibrated' -e 'covering' -e 'out of the' data/0/md.log;"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Verify that the covering times are shorter than in the single-walker case above. Thus, the free energy update size decreases more rapidly,  This is because the sampling interval is now covered *cooperatively* by all the walkers. Here a covering means that each point in the interval `awh1-dim1-start` to `awh1-dim1-end` has been sampled (with sufficient probability) by one of the walkers. We can visualize what's going on by looking at the RC trajectories.\n",
    "<a id='multisim_plot'></a>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# Extract substrings using regular expressions\n",
    "import re\n",
    "\n",
    "nwalkers=2\n",
    "rundirs=[\"data/\" + str(i) for i in range(nwalkers)]\n",
    "\n",
    "# Data per walker\n",
    "for rund in rundirs:\n",
    "    # Read pullx.xvg file and plot a selected part of the trajectory\n",
    "    data = read_xvg(rund + \"/pullx.xvg\")\n",
    "    t, x = data[:,0], data[:,1]\n",
    "    plt.plot(t, x, lw=0.5)\n",
    "\n",
    "# Set y-axis limits based on the chosen sampling interval \n",
    "awh_start, awh_end = 0.25, 0.65    \n",
    "plt.ylim([awh_start, awh_end])\n",
    "ymin, ymax = plt.gca().get_ylim()\n",
    "\n",
    "# Find covering times and plot them (note that these are shared for the walkers)\n",
    "covering_logs = !grep -e covered -e covering {rundirs[0]}/md.log\n",
    "covering_times = [float(re.findall('t = (.+?) ps', log)[0]) for  log in covering_logs]\n",
    "\n",
    "for tcover, log in zip(covering_times, covering_logs):\n",
    "    if 'Decreased the update size'  in log:\n",
    "        ls='-'\n",
    "    else:\n",
    "        # If the histogram equilibration was required (by mdp option 'awh1-equilibrate-histogram = true' )\n",
    "        # then the covering will not take effect, i.e. update size will not decrease until the sampling interval \n",
    "        # has been sampled uniformly \"enough\".\n",
    "        ls='--'\n",
    "                \n",
    "    plt.plot([tcover]*2,[ymin,ymax], color='k', lw=2, ls=ls)\n",
    "\n",
    "# Also mark the exit time if there was one\n",
    "exit_logs = !grep 'out of the' {rundirs[0]}/md.log\n",
    "if len(exit_logs) > 0:\n",
    "    texit = float(re.findall('t = (.+?)', exit_logs[0])[0])\n",
    "    plt.plot([texit],0.5*(ymin + ymax), color='k', marker='x', ms=10)\n",
    "\n",
    "# Tweak more\n",
    "tend=tcover*2\n",
    "plt.xlim([0,min(tend,max(t))])\n",
    "plt.xlabel(\"time (ps)\")\n",
    "plt.ylabel(\"distance (nm)\");\n",
    "plt.title('RC trajectory');"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The dashed line denotes the first actual covering. Leading question: is the histogram still biased by the initial conditions at this time? Since we had set `awh1-equilibrate-histogram = true` in the .mdp file the detection of covering won't take effect until the accumulating shared histogram has become uniform enough, which happens at the first solid line. Pre-equilibrating the histogram this way is nothing but a heuristic to make the walkers spread out initially and loose the \"memory\" of their initial configuration before attempting to converge the free energy!  \n",
    "\n",
    "Verify also that now a \"covering\" is not equal to a full end-to-end transition as it was in the single-walker case. Each walker just has to transition across part of the full sampling interval. That is why the initial stage becomes shorter.\n",
    "\n",
    "To summarize: running multiple walkers has the potential of improving the intitial convergence. But remember, to obtain statistical error estimates you would generally generate *multiple* multiwalker simulation, so don't burn all the compute time at once!"
   ]
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.12.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
